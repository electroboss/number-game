#include <stdio.h>
int number = 0;
int maxnum;
int delta;
int offset;

int main();
int move();
int playermove();
int check(int add);

int main(){
  char starter[1];
  printf("Losing number: ");
  scanf("%d", &maxnum);
  printf("Sayable numbers: ");
  scanf("%d", &delta);
  printf("Who starts? p=player,c=computer: ");
  scanf("%s", &starter);

  int add;
  if (starter[0] == 'c'){
    // AI move
    add = move();
    if (check(add)){
      number += add;
    } else {
      printf("Disallowed number from AI: %d\n", add);
      return 1;
    }
  }

  while (1){
    // Player move
    add = playermove();
    if (check(add)){
      number += add;
    } else {
      printf("Disallowed number from Player: %d\n", add);
      return 1;
    }

    if (number >= maxnum) {
      printf("AI wins\n");
      return 0;
    }

    // AI move
    add = move();
    if (check(add)){
      number += add;
    } else {
      printf("Disallowed number from AI: %d\n", add);
      return 1;
    }

    if (number >= maxnum) {
      printf("You win\n");
      return 0;
    }
  }

  return 0;
}

int check(int add){
  return (add > 0 && add <= delta);
}

int move(){
  int add = ((maxnum-1)%(delta+1)-(number%(delta+1))+(delta+1))%(delta+1);
  if (add <= 0 || add > delta) {
    add = 1;
  }
  return add;
}

int playermove(){
  printf("========================\n");
  printf("Number: %d\n",number);
  printf("Choices: \n");
  for (int i=1; i<=delta; i++){
    printf("%d: %d\n", i, number+i);
  }
  printf("Which do you choose? (1-%d): ", delta);

  int choice;
  scanf("%d", &choice);

  return choice;
}